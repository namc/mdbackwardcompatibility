# MDbackwardcompatibility

Backward compatibility of Material Theme. 


1. Defined themes that inherits from older theme in  res/values
2. Defined themes with the same name that inherits from the material theme in res/values-v21
3. Used v7 appcompat library
